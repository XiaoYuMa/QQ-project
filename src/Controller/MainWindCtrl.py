#!/usr/bin/python3.6
# -*- coding: utf-8 -*-
# @Time    : 2020/9/1 14:37
# @Author  : zhang shuai
# @Email   : 310966164@qq.com
# @File    : MainWindCtrl.py
# @Software: PyCharm
import sys

from PyQt5 import QtWidgets

from src.UI.MainWindowUI import MainWindowUI


class MainWindowController:
    UI = None

    def __init__(self):
        self.__startWindow()

    def __startWindow(self):
        # self.MainWindow = QtWidgets.QMainWindow()
        # self.UI = MainWindowUI()
        # self.UI.setupUi(self.MainWindow)
        # self.MainWindow.show()
        # self.center()

        app = QtWidgets.QApplication(sys.argv)
        self.MainWindow = QtWidgets.QMainWindow()
        self.UI = MainWindowUI()
        self.UI.setupUi(self.MainWindow)
        self.MainWindow.show()
        self.center()
        sys.exit(app.exec_())

    # 设置页面居中显示
    def center(self):
        winRectangle = self.MainWindow.frameGeometry()  # 这是得到窗口矩形
        centerPoint = QtWidgets.QDesktopWidget().availableGeometry().center()  # 获取屏幕的中心点坐标
        winRectangle.moveCenter(centerPoint)  # 将窗口同型矩形
        self.MainWindow.move(winRectangle.topLeft())