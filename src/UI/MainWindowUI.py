# -*- coding: utf-8 -*-
'''
# @Time    : 2020/7/21 23:56
# @Author  : Zhangshuai
# @Site    : 
# @File    : MainWindowUI.py
# @Software: PyCharm
'''

from PyQt5 import QtCore, QtGui, QtWidgets
import sys

from PyQt5.QtGui import QPalette, QBrush, QPixmap
from PyQt5.QtWidgets import QMainWindow


class MainWindowUI(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()
        self.setObjectName("Form")
        self.resize(345, 746)
        self.headCapture = QtWidgets.QLabel(self)
        self.headCapture.setGeometry(QtCore.QRect(10, 10, 51, 51))
        self.headCapture.setObjectName("headCapture")
        self.userName = QtWidgets.QLabel(self)
        self.userName.setGeometry(QtCore.QRect(70, 10, 91, 16))
        self.userName.setObjectName("userName")
        self.listWidget = QtWidgets.QListWidget(self)
        self.listWidget.setGeometry(QtCore.QRect(10, 170, 321, 561))
        self.listWidget.setStyleSheet("QListWidget{\n"
                                      "    font-size:20px;\n"
                                      "}")
        self.listWidget.setObjectName("listWidget")
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)

        self.setBackground()
        self.retranslateUi(self)

        QtCore.QMetaObject.connectSlotsByName(self)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.headCapture.setText(_translate("Form", "头像"))
        self.userName.setText(_translate("Form", "昵称"))
        __sortingEnabled = self.listWidget.isSortingEnabled()
        self.listWidget.setSortingEnabled(False)
        item = self.listWidget.item(0)
        item.setText(_translate("Form", "幻海潮汐"))
        item = self.listWidget.item(1)
        item.setText(_translate("Form", "陌上花开"))
        item = self.listWidget.item(2)
        item.setText(_translate("Form", "凌波微步"))
        item = self.listWidget.item(3)
        item.setText(_translate("Form", "大怒额嗯尺寸"))
        item = self.listWidget.item(4)
        item.setText(_translate("Form", "二位炽热v"))
        item = self.listWidget.item(5)
        item.setText(_translate("Form", "v绿萼vv他"))
        item = self.listWidget.item(6)
        item.setText(_translate("Form", "而非我如果投保人"))
        item = self.listWidget.item(7)
        item.setText(_translate("Form", "发热管贴吧"))
        item = self.listWidget.item(8)
        item.setText(_translate("Form", "rv\'re\'b\'t"))
        item = self.listWidget.item(9)
        item.setText(_translate("Form", "v热不痛不痒"))
        item = self.listWidget.item(10)
        item.setText(_translate("Form", "verb\'t\'r\'b"))
        item = self.listWidget.item(11)
        item.setText(_translate("Form", "v儿童保育院"))
        item = self.listWidget.item(12)
        item.setText(_translate("Form", "v热Bryn"))
        item = self.listWidget.item(13)
        item.setText(_translate("Form", "v\'t\'run"))
        item = self.listWidget.item(14)
        item.setText(_translate("Form", "v突然努特"))
        item = self.listWidget.item(15)
        item.setText(_translate("Form", "太湖积木"))
        item = self.listWidget.item(16)
        item.setText(_translate("Form", "有奴役ik"))
        self.listWidget.setSortingEnabled(__sortingEnabled)

    # 设置界面背景
    def setBackground(self):
        self.setAutoFillBackground(True)
        palette = QPalette()
        palette.setBrush(self.backgroundRole(), QBrush(QPixmap('F:/QQ-project/source/pifu.jpg')))
        self.setPalette(palette)

# if __name__ == '__main__':
#
#     app = QtWidgets.QApplication(sys.argv)
#     widget = QtWidgets.QWidget()
#     ui = MainWindowUI()
#     ui.show()
#     sys.exit(app.exec_())