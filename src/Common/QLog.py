#!/usr/bin/python3.6
# -*- coding: utf-8 -*-
# @Time    : 2020/9/1 15:53
# @Author  : zhang shuai
# @Email   : 310966164@qq.com
# @File    : QLog.py
# @Software: PyCharm
import logging
import os
from logging.handlers import TimedRotatingFileHandler
import colorlog


log_color_config = {
    'DEBUG': 'cyan',
    'INFO': 'green',
    'WARNING': 'yellow',
    'ERROR': 'red',
    'CRITICAL': 'red'
}

LogLevel = "DEBUG"  # 日志级别
Log = logging.getLogger("Log")  # 获取日志对象
Log.setLevel(LogLevel)  # 设置日志级别 DEBUG, INFO, WARNING, ERROR, CRITICAL
TimeFileHandler = None
StreamHandler = None

def setLogHandle():
    global LogLevel, Log, TimeFileHandler, StreamHandler
    LogPath = '{}{}'.format('F:/QQ-project/log/', 'Log.txt')
    logFold = os.path.dirname(LogPath)
    if False is os.path.isdir(logFold):
        os.makedirs(logFold)

    TimeFileHandler = TimedRotatingFileHandler(LogPath, when='H', encoding="utf-8", backupCount=8760)
    # when ： S、M、H、D、W0-W6（W0=星期一）midnight
    TimeFileHandler.setLevel(LogLevel)
    if "DEBUG" == LogLevel:
        formatter = colorlog.ColoredFormatter("%(log_color)s[%(asctime)s] [%(levelname)s] [%(filename)s] [%(funcName)s (%(lineno)d)]: %(message)s ", log_colors=log_color_config, style='%')
    else:
        formatter = logging.Formatter("[%(asctime)s] [%(levelname)s] [%(filename)s] [%(funcName)s (%(lineno)d)]: %(message)s")
    TimeFileHandler.setFormatter(formatter)
    Log.addHandler(TimeFileHandler)

    if "DEBUG" == LogLevel:
        StreamHandler = logging.StreamHandler()
        StreamHandler.setLevel(LogLevel)
        StreamHandler.setFormatter(formatter)
        Log.addHandler(StreamHandler)

setLogHandle()