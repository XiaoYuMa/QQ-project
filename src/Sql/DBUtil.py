#!/usr/bin/python3.6
# -*- coding: utf-8 -*-
# @Time    : 2020/7/8 16:31
# @Author  : zhang shuai
# @Email   : 310966164@qq.com
# @File    : DBUtil.py
# @Software: PyCharm
import json
import re
import time
import urllib

import pymysql
import requests

from src.Common.QLog import Log


class DataBaseModel():
    __conn = None
    __cursor = None

    def __init__(self):
        # 创建数据库链接
        self.__conn = pymysql.connect(host='localhost',
                                      user='root',
                                      password='ZS871220..',
                                      database='qqdb')
        self.__cursor = self.__conn.cursor()


    # 查询用户名是否存在
    def checkUser(self, userName):
        try:
            sql = "SELECT COUNT(*) FROM user_info WHERE username={};".format(userName)
            self.__cursor.execute(sql)
            values = self.__cursor.fetchall()
            if values[0][0] == 1:
                return True
            else:
                return False
        except Exception as e:
            Log.error('查询用户名发生异常{}'.format(e))

    # 查询用户密码
    def checkPassword(self, userName):
        try:
            sql = "SELECT password FROM user_info WHERE username={};".format(userName)
            self.__cursor.execute(sql)
            values = self.__cursor.fetchall()
            if len(values) > 0:
                return values[0][0]
        except Exception as e:
            Log.error('查询用户密码发生异常{}'.format(e))

